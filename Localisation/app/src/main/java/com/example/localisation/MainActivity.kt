package com.example.localisation


import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices


class MainActivity : AppCompatActivity(){
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var location1: TextView
    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        location1= findViewById(R.id.location)
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
             location1.text = location!!.latitude.toString()
            }
    }


}
