import 'package:flutter/material.dart';
import 'package:kalinews/models/category.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CategoryItem extends StatelessWidget {
  final Category _category;
  //final int _index;
 final double _fontSize = 13.0;
  final String _fontFamilly = "Roboto";
  CategoryItem(this._category);
  Text _catString(Category cat) {

    if (cat.categoryType ==  CategoryType.TECH) {
      return Text("#Tech",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: _fontSize,fontFamily: _fontFamilly),);
    }
    if (cat.categoryType ==  CategoryType.SPORT) {
      return Text("#Sport",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: _fontSize,fontFamily: _fontFamilly),);
    }
    if (cat.categoryType ==  CategoryType.DIVERTISSEMENT) {
      return Text("#Divers",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: _fontSize,fontFamily: _fontFamilly),);
    }
    if (cat.categoryType ==  CategoryType.DESIGN) {
      return Text("#Design",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: _fontSize, fontFamily: _fontFamilly),);
    }
    if (cat.categoryType ==  CategoryType.SCIENCE) {
      return Text("#Science",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: _fontSize,fontFamily: _fontFamilly),);
    }
    if (cat.categoryType ==  CategoryType.SANTE) {
      return Text("#Santé",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: _fontSize, fontFamily: _fontFamilly),);
    }
    if (cat.categoryType ==  CategoryType.POLITIQUE) {
      return Text("#Politique",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: _fontSize, fontFamily: _fontFamilly),);
    }
    if (cat.categoryType ==  CategoryType.MARKETING) {
      return Text("#Marketing",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: _fontSize, fontFamily: _fontFamilly),);
    }
    if (cat.categoryType ==  CategoryType.FINANCE) {
      return Text("#Finance",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold,fontSize: _fontSize,fontFamily: _fontFamilly),);
    }
    else {
      return Text("");
    }
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Container(
        padding: EdgeInsets.all(4.0),
        margin: EdgeInsets.symmetric(horizontal: 2.0, vertical: 2.0),
        height: 100.0,
        width: 100.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          image: DecorationImage(image: AssetImage("images/default.png"),
          fit: BoxFit.cover)
        ),
        child: Stack(
          children: <Widget>[
            Positioned(
              bottom: 0,
              left: 1,
              child: Text("#${_category.categoryType}",
              style:  TextStyle(color: Colors.white, fontWeight: FontWeight.bold,fontSize: _fontSize,fontFamily: _fontFamilly),),
            ),
           Center(
             child:CircleAvatar(
               radius: 15.0,
               backgroundColor: _category.categoryState ? Colors.transparent :Colors.transparent,
               child: Center(
                 child: _category.categoryState ? SvgPicture.asset("images/success.svg"): null,
               )
             )
           )
          ],
        )
    );


    /*RawMaterialButton(
        child: Text(_category.categoryType.toString(), style: TextStyle(color: Colors.white),),
        fillColor: _category.categoryState ? Colors.red :Colors.black,
        onPressed: null);*/
  }
}