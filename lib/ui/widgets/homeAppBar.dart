import 'package:flutter/material.dart';

AppBar defineAppBar(String title, {bool showback = false}) {
  return AppBar(
    automaticallyImplyLeading: showback,
    backgroundColor: Colors.white,
    title: Text(title.toUpperCase(), style: TextStyle(color: Colors.black, fontSize: 16.0, fontFamily: "Roboto", fontWeight: FontWeight.w300),),
    centerTitle: false,
    elevation: 2.0,
    iconTheme: IconThemeData(
      color: Colors.black
    ),
    actions: <Widget>[
      IconButton(
          icon: Icon(Icons.search, color: Colors.black,),
          onPressed: () {
            print("recherche");
          })
    ],
  );
}