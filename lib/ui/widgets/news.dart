import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kalinews/models/post.dart';

class NewsCard extends StatelessWidget {
  final Post news;
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  NewsCard(this.news);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 18.0,),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(news.title,
                      maxLines: 3,
                      style: TextStyle(color: Colors.black, fontSize: 14.0, height: 1.2,
                      fontFamily: 'Roboto'),
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(height: 10.0,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Opacity(
                                    opacity: .7,
                                    child: Text("il y a ${news.time}h",
                                      style: TextStyle(fontFamily: 'Roboto-Light-S',
                                          fontSize: 10.0
                                      ),
                                      textAlign: TextAlign.left,)
                                ),
                                SizedBox(width: 5.0,),
                                SvgPicture.asset("images/ellipse.svg"),
                                SizedBox(width: 5.0,),
                                Opacity(
                                    opacity: .7,
                                    child: Text("${news.source}",
                                      style: TextStyle(fontFamily: 'Roboto-Light-S',
                                          fontSize: 10.0
                                      ),
                                      textAlign: TextAlign.left,)
                                ),

                                SizedBox(width: 5.0,),
                                SvgPicture.asset("images/ellipse.svg"),
                                SizedBox(width: 5.0,),
                                Opacity(
                                    opacity: .7,
                                    child: Text("${capitalize(news.category)}",
                                      style: TextStyle(fontFamily: 'Roboto-Light-S',
                                          fontSize: 10.0
                                      ),
                                      textAlign: TextAlign.left,)
                                ),

                              ],
                            ) ),
                      ],
                    )
                  ],
                )
            ),
            SizedBox(width: 15.0,),
            Container(
              height: 80,
              width: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(3.0),
                color: Theme.of(context).primaryColor,
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: news.imageUrl != null ?  NetworkImage(news.imageUrl) : AssetImage("images/default.png"))
              ),
            )
          ],
    ),
    );
  }
}