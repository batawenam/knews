import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

AppBar detailsAppBar(String title,) {
  return AppBar(
    backgroundColor: Colors.white,
    iconTheme: IconThemeData(
      color: Colors.black,
    ),
    title: Text(title.toUpperCase(), style: TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "Roboto", fontWeight: FontWeight.w300),),
    centerTitle: false,
    elevation: 2.0,
    actions: <Widget>[
      IconButton(
          icon:SvgPicture.asset("images/favoris_icon.svg"),
            color: Colors.black,
          onPressed: () {
            print("favaris");
          }),
      IconButton(
          icon: SvgPicture.asset("images/share_icon.svg"),
            color: Colors.black,
          onPressed: () {
            print("favaris");
          })
    ],
  );
}