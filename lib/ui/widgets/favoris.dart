import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kalinews/models/post.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

class FavorisCard extends StatelessWidget {
  final databaseReference = FirebaseDatabase.instance.reference();
  final Post news;
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  FavorisCard(this.news);
  void deleteFavorites() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String uid = user.uid.toString();
    await databaseReference.child("/users/$uid/favorites/${news.id}").remove();
    await databaseReference.child("/users/$uid/favoritesId/${news.id}").remove();
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(
          vertical: 18.0,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 250,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  image: DecorationImage(
                      image: NetworkImage(news.imageUrl), fit: BoxFit.cover),
                  borderRadius: BorderRadius.circular(5.0)),
            ),
           SizedBox(height: 20.0,),
           Row(
             mainAxisAlignment: MainAxisAlignment.start,
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
               Expanded(
                 child:Column(
                   mainAxisAlignment: MainAxisAlignment.start,
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[
                     Text(
                       news.title,
                       maxLines: 3,
                       style: TextStyle(
                           color: Colors.black,
                           fontSize: 14.0,
                           height: 1,
                           fontFamily: 'Roboto'),
                       textAlign: TextAlign.left,
                     ),
                     SizedBox(height: 5.0,),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.start,
                       children: <Widget>[
                         Opacity(
                             opacity: .7,
                             child: Text(
                               "il y a ${news.time}h",
                               style: TextStyle(
                                   fontFamily: 'Roboto-Light-S', fontSize: 10.0),
                               textAlign: TextAlign.left,
                             )),
                         SizedBox(
                           width: 5.0,
                         ),
                         SvgPicture.asset("images/ellipse.svg"),
                         SizedBox(
                           width: 5.0,
                         ),
                         Opacity(
                             opacity: .7,
                             child: Text(
                               "${news.source}",
                               style: TextStyle(
                                   fontFamily: 'Roboto-Light-S', fontSize: 10.0),
                               textAlign: TextAlign.left,
                             )),
                         SizedBox(
                           width: 5.0,
                         ),
                         SvgPicture.asset("images/ellipse.svg"),
                         SizedBox(
                           width: 5.0,
                         ),
                         Opacity(
                             opacity: .7,
                             child: Text(
                               "${capitalize(news.category)}",
                               style: TextStyle(
                                   fontFamily: 'Roboto-Light-S', fontSize: 10.0),
                               textAlign: TextAlign.left,
                             )),
                       ],
                     ),
                   ],
                 )
               ),
               SizedBox(height: 10.0,),
               IconButton(
                 icon: SvgPicture.asset("images/isFavorites.svg"),
                        onPressed: ()  {
                      deleteFavorites();
                    })
                   ],
           )
           /* Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        news.title,
                        maxLines: 3,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14.0,
                            height: 1.2,
                            fontFamily: 'Roboto'),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Opacity(
                            opacity: .7,
                            child: Text(
                              "il y a ${news.time}h",
                              style: TextStyle(
                                  fontFamily: 'Roboto-Light-S', fontSize: 10.0),
                              textAlign: TextAlign.left,
                            )),
                        SizedBox(
                          width: 5.0,
                        ),
                        SvgPicture.asset("images/ellipse.svg"),
                        SizedBox(
                          width: 5.0,
                        ),
                        Opacity(
                            opacity: .7,
                            child: Text(
                              "${news.source}",
                              style: TextStyle(
                                  fontFamily: 'Roboto-Light-S', fontSize: 10.0),
                              textAlign: TextAlign.left,
                            )),
                        SizedBox(
                          width: 5.0,
                        ),
                        SvgPicture.asset("images/ellipse.svg"),
                        SizedBox(
                          width: 5.0,
                        ),
                        Opacity(
                            opacity: .7,
                            child: Text(
                              "${capitalize(news.category)}",
                              style: TextStyle(
                                  fontFamily: 'Roboto-Light-S', fontSize: 10.0),
                              textAlign: TextAlign.left,
                            )),
                      ],
                    )),
                    SizedBox(width: 20.0,),
                    IconButton(
                        icon: SvgPicture.asset("images/isFavorites.svg"),
                        onPressed: ()  {
                          print("images/isFavorites.svg");
                        })
                  ],
                )
              ],
            )*/
          ],
        )
        );
  }
}
