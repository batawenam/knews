import 'package:flutter/material.dart';
import 'package:kalinews/ui/widgets/homeAppBar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:kalinews/service/checklogged.dart';
class ProfilePage extends StatefulWidget {
  ProfilePageState createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
  FirebaseUser user;
  CircleAvatar _buildLogo () {
    return CircleAvatar(
      backgroundColor: Colors.transparent,
      backgroundImage: AssetImage("images/google.png"),
      radius: 10.0,
    );
  }
  Opacity _buildText() {
    return Opacity(
      opacity:1,
      child: Text(
        "Déconnexion",
        style: TextStyle(
            fontSize: 18.0,
            color: Colors.white
        ),
      ),

    );
  }
  void inputData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    if(user != null ) {
      setState(() {
        this.user = user;
      });
    }
    else return ;

  }
  @override
  Widget build(BuildContext context) {
    inputData();
    return Scaffold(
      backgroundColor:Color.fromRGBO(242, 242, 242, 1) ,
      appBar: defineAppBar("PROFIL"),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              height: 300,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,

                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 28.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(height: 20.0,),
                            CircleAvatar(
                              radius: 40,
                              //  backgroundColor: Theme.of(context).primaryColor,
                              backgroundImage: user != null ? NetworkImage(user.photoUrl) : AssetImage("images/profil.png"),
                            ),
                            Text(user != null ? user.displayName : "", style: TextStyle(fontFamily: "Roboto", fontWeight: FontWeight.bold, fontSize: 22.0, color: Colors.black),),
                            SizedBox(height: 4.0,),
                            Opacity(
                              opacity: .6,
                              child: Text(user != null ? user.email: "", style: TextStyle(fontFamily: "Roboto",fontSize: 10.0, color: Colors.black),),),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child:  MaterialButton(
                        height: 60.0,
                        elevation: 5.0,
                        onPressed:  ()  async {
                          await FirebaseAuth.instance.signOut();
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => CheckCurrentUser()));
                        },
                        color: Theme.of(context).primaryColor,
                        shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                        child:SizedBox(
                          width: double.infinity,
                          child:  Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                               Center(
                                 child: _buildText(),
                               )
                            ],
                          ),
                        )
                    ),
                  )
                ],
              )
          ),
          SizedBox(height: 100.0,),
          Center(
            child: Opacity(
                opacity: .7,
             child: Text("Powered by"),),
          ),
          Center(
            child: Opacity(
              opacity: .7,
              child: Text("41DEVS", style: TextStyle(fontSize: 22,fontFamily: "Roboto" ),),
            ),
          ),
        ],
      )
    );
  }

}