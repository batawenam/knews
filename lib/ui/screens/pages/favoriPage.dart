import 'package:flutter/material.dart';
import 'package:kalinews/ui/widgets/homeAppBar.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:kalinews/ui/widgets/favoris.dart';
import 'package:kalinews/models/post.dart';
import 'package:kalinews/ui/screens/pages/detailsPage.dart';
class FavorisPage extends StatefulWidget {
  FavorisPageState createState() => FavorisPageState();
}

class FavorisPageState extends State<FavorisPage> {
  final databaseReference = FirebaseDatabase.instance.reference();
  List<Post> allFavoris;
  bool dataIsReady = false;
  void getAllFavoris() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String uid = user.uid;
    List<Post> post = new List();
    await databaseReference.child("/users/$uid/favorites").once().then((DataSnapshot dataSnapshot)
    {
      if(dataSnapshot.value != null){
        for (var value in dataSnapshot.value.values) {
          if(value != null) {
            post.add(Post.fromJson(value));
          }
        }
      } else {
        post= [];
      }
    });
    setState(() {
      allFavoris = post;
      dataIsReady = true;
    });
  }
  Widget _buildEmpty() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.star_border,
          size: 200.0,),
          Opacity(
            opacity: .5,
            child: Text(
              "Aucun favoris pour l’instant",
              style: TextStyle(
                fontFamily: "Roboto",
                fontSize: 15.0
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    getAllFavoris();
    return Scaffold(
      appBar: defineAppBar("ARTICLES FAVORIS"),
      body: !dataIsReady ? Center(
        child: CircularProgressIndicator(
          backgroundColor: Theme.of(context).primaryColor,
        ),
      ) :  allFavoris.length == 0 ? _buildEmpty() : Padding(
        padding: EdgeInsets.symmetric(horizontal: 0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: ListView.builder(
                    itemCount:allFavoris.length,
                    itemBuilder: (context, i) {
                      return FlatButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context)  {
                            return DetailsPage(news: allFavoris[i],);
                          }
                          ));
                        },
                        child: FavorisCard(allFavoris[i]),
                        color: Colors.white,);
                    }))
          ],
        ),
      )
    );
  }

}