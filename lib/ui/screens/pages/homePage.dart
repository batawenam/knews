import 'package:flutter/material.dart';
import 'package:kalinews/ui/widgets/homeAppBar.dart';
import 'package:kalinews/ui/widgets/news.dart';
import 'detailsPage.dart';
import 'package:kalinews/models/post.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';

class CustomHome extends StatefulWidget {
  CustomHomeState createState() => CustomHomeState();
}
class CustomHomeState extends State<CustomHome> {
  final databaseReference = FirebaseDatabase.instance.reference();
  List<String> userCategory;
  List<Post> allNews = new List();
  List<Post> customNews = new List();
  void getTags() async {
    List<String> tags = new List();
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String uid = user.uid.toString();
    await databaseReference.child("/users/$uid/tags").once().then((DataSnapshot dataSnapshot) {
      for (var value in dataSnapshot.value) {
        if(value != null) {
          tags.add(value.toString());
        }
      }
    });
    setState(() {
      userCategory = tags;
    });
  }
  void getAllNews() async {
    List<Post> post = new List();
    await databaseReference.child("/news").once().then((DataSnapshot dataSnapshot)
    {
        for (var value in dataSnapshot.value.values) {
          if(value != null) {
            post.add(Post.fromJson(value));
          }
        }
    });
    setState(() {
      allNews = post;
      dataIsReady = true;
    });
  }
  bool dataIsReady = false;
  void getCustomNews() async {
    getTags();
    getAllNews();

     setState(() {
       customNews = allNews.where((item) => userCategory.contains(item.category)).toList();
     });
  }
  @override
  Widget build(BuildContext context) {
    getCustomNews();
    // TODO: implement build
    return Scaffold(
      appBar: defineAppBar("Sélectionnés pour vous"),
      body: !dataIsReady ? Center(
        child: CircularProgressIndicator(
          backgroundColor: Theme.of(context).primaryColor,
        ),
      ) : Padding(
        padding: EdgeInsets.symmetric(horizontal: 0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: ListView.builder(
                    itemCount:customNews.length,
                    itemBuilder: (context, i) {
                      return FlatButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context)  {
                            return DetailsPage(news: customNews[i],);
                          }
                          ));
                        },
                        child:NewsCard(customNews[i]),
                        color: Colors.white,);
                    }))
          ],
        ),
      )
      /*Padding(
          padding: EdgeInsets.symmetric(horizontal: 0.0),
         child: Column(
           mainAxisAlignment: MainAxisAlignment.start,
           children: <Widget>[
             Expanded(
                 child: ListView.builder(
                     itemCount:allNews == null ? 0 : allNews.length,
                     itemBuilder: (context, i) {
                       return FlatButton(
                           onPressed: () {
                             Navigator.of(context).push(MaterialPageRoute(builder: (context)  {
                               return DetailsPage(allNews[i]);
                             }
                             ));
                           },
                           child: NewsCard(allNews, i),
                           color: Colors.white,);
                     }))
           ],
         ),
      )*/
    );
  }
}

