import 'package:flutter/material.dart';
import 'package:kalinews/models/category.dart';
import 'package:kalinews/utils/categories.dart';
import 'package:kalinews/ui/widgets/categoryGridItem.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:kalinews/ui/widgets/homeAppBar.dart';
import 'catList.dart';

class CategoryPage extends StatefulWidget {
  CategoryPageState createState() => CategoryPageState();
}

class CategoryPageState extends State<CategoryPage> {
  final List<Category> _categories = getCategory();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    Widget newBuild = StaggeredGridView.countBuilder(
      crossAxisCount: 4,
      itemCount: _categories.length,
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
      itemBuilder: (BuildContext context, int index) => new Container(
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => CatHome(categoryType: _categories[index].categoryType,)));
          },
          child: Container(
            child: CategoryItem( _categories[index]),
          ),
        ) ,
      ),
      staggeredTileBuilder: (int index) =>
      new StaggeredTile.count(2, index.isEven ? 2 : 3,),
      mainAxisSpacing: 6.0,
      crossAxisSpacing: 6.0,
    );
    /*   Widget buildSelectionGridView = GridView.count(
      childAspectRatio: (itemWidth / itemHeight),
      crossAxisCount: 2,
        controller: new ScrollController(keepScrollOffset: false),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
     padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
      children: List.generate(_categories.length, (index) {
        return index % 2 != 0 ? Container(
          margin: EdgeInsets.only(top: 20.0),
          child: GestureDetector(
            onTap: () {
              setState(() {
                _categories[index].categoryState =  !_categories[index].categoryState;
                pushSelectedCategory( _categories[index]);
              });
            },
            child: Container(
              child: CategoryItem( _categories[index]),
            ),
          ) ,
        )

        : Container(
          child:  GestureDetector(
            onTap: () {
              setState(() {
                _categories[index].categoryState =  !_categories[index].categoryState;
                pushSelectedCategory( _categories[index]);
              });
            },
            child: CategoryItem( _categories[index]),
          ),
        );
      })
      *//* _categories.map((category) {
        return GestureDetector(
          onTap: () {
            print(category);
            setState(() {
              category.categoryState = !category.categoryState;
              pushSelectedCategory(category);
            });
          },
          child: CategoryItem(category)
        );
      }).toList(),*//*
    );*/
    return Scaffold(
        appBar: defineAppBar("Catégories"),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: newBuild)
          ],
        )
        );
  }

}