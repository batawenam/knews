import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:share/share.dart';
import 'package:kalinews/models/post.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:kalinews/ui/screens/browser/newsViews.dart';
class DetailsPage extends StatefulWidget {
  final Post news;
  const DetailsPage({Key key, this.news}): super(key: key);
  DetailPageState createState() => DetailPageState();
}
class DetailPageState extends State<DetailsPage> {
  bool isFavorites = false;
  final databaseReference = FirebaseDatabase.instance.reference();
  List<String> userFavorites;
  void deleteFavorites() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String uid = user.uid.toString();
    await databaseReference.child("/users/$uid/favorites/${widget.news.id}").remove();
    await databaseReference.child("/users/$uid/favoritesId/${widget.news.id}").remove();
  }
  void addFavorites() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String uid = user.uid.toString();
    await databaseReference.child("/users/$uid/favorites/${widget.news.id}").set(
        {
          'category': widget.news.category,
          'content': widget.news.content,
          'id': widget.news.id,
          'imageUrl': widget.news.imageUrl,
          'source': widget.news.source,
          'time': widget.news.time,
          'title': widget.news.title,
          'url': widget.news.url
        }
    );
    await databaseReference.child("/users/$uid/favoritesId/${widget.news.id}").set(
      {
        'id': '${widget.news.id}'
      }
    );
  }
  void getUserFavorites() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String uid = user.uid.toString();
    await databaseReference.child("/users/$uid/favoritesId").once().then((DataSnapshot dataSnapshot) {
      List<String> favorite = new List();
      for (var value in dataSnapshot.value.values) {
        if(value != null) {
          favorite.add(value['id'].toString());
        }
      }
      print(favorite);
      setState(() {
        userFavorites = favorite;
        if(userFavorites.contains(widget.news.id)) {
          isFavorites = true;
        } else {
          isFavorites = false;
        }
      });
    });
  }

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  @override
  Widget build(BuildContext context) {
    getUserFavorites();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: Text("POUR MOI", style: TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "Roboto", fontWeight: FontWeight.w300),),
        centerTitle: false,
        elevation: 2.0,
        actions: <Widget>[
          IconButton(
              icon:!isFavorites ?  SvgPicture.asset("images/favoris_icon.svg") : SvgPicture.asset("images/isFavorites.svg"),
              color: Colors.black,
              onPressed: () {
                if(!isFavorites) {
                  addFavorites();
                } else {
                  deleteFavorites();
                }
              }),
          IconButton(
              icon: SvgPicture.asset("images/share_icon.svg"),
              color: Colors.black,
              onPressed: () {
                Share.share(widget.news.url);
              })
        ],
      ),
      body:SingleChildScrollView(
        child:  Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 250,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    image: DecorationImage(
                        image: NetworkImage(widget.news.imageUrl),
                        fit: BoxFit.cover
                    ),
                  borderRadius: BorderRadius.circular(5.0)
                ),
              ),
              SizedBox(height: 10.0,),
              Text(widget.news.title,
                maxLines: 3,
                style: TextStyle(color: Colors.black, fontSize: 15.0, height: 1,
                    fontFamily: 'Roboto'),
                textAlign: TextAlign.left,
              ),
             SizedBox(height: 10.0,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Opacity(
                              opacity: .7,
                              child: Text("il y a ${widget.news.time}h",
                                style: TextStyle(fontFamily: 'Roboto-Light-S',
                                    fontSize: 10.0
                                ),
                                textAlign: TextAlign.left,)
                          ),
                          SizedBox(width: 5.0,),
                          SvgPicture.asset("images/ellipse.svg"),
                          SizedBox(width: 5.0,),
                          Opacity(
                              opacity: .7,
                              child: Text("${widget.news.source}",
                                style: TextStyle(fontFamily: 'Roboto-Light-S',
                                    fontSize: 10.0
                                ),
                                textAlign: TextAlign.left,)
                          ),

                          SizedBox(width: 5.0,),
                          SvgPicture.asset("images/ellipse.svg"),
                          SizedBox(width: 5.0,),
                          Opacity(
                              opacity: .7,
                              child: Text("${capitalize(widget.news.category)}",
                                style: TextStyle(fontFamily: 'Roboto-Light-S',
                                    fontSize: 10.0
                                ),
                                textAlign: TextAlign.left,)
                          ),

                        ],
                      ) ),
                ],
              ),
             SizedBox(height: 10.0,),
            Opacity(
                      opacity: .9,
                      child: Text(widget.news.content,
                        style: TextStyle(fontFamily: 'Roboto-Light-S',
                            fontSize: 13.0,
                        ),
                        maxLines: 20,
                        textAlign: TextAlign.justify,)
                  ),
              SizedBox(height: 10.0,),
            MaterialButton(
              height: 60.0,
              elevation: 5.0,
              onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => MyApps()));
             },
               color:  Theme.of(context).primaryColor,
               shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                child: Center(child: Text("LIRE PLUS", style: TextStyle(color: Colors.white ),),
            )
        )
            ],
          ),
        ),
      )
    );
  }
}