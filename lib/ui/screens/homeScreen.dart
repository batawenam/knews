import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kalinews/ui/screens/pages/homePage.dart';
import 'package:kalinews/ui/screens/pages/categoriesPage.dart';
import 'package:kalinews/ui/screens/pages/favoriPage.dart';
import 'package:kalinews/ui/screens/pages/profilePage.dart';
import 'package:flutter/services.dart';
import 'package:kalinews/service/firebaseAuth.dart';

class HomePage extends StatefulWidget {
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  final ProfilePage _profilePage =  ProfilePage();
  final CustomHome _customHome =  CustomHome();
  final CategoryPage _categoriesPage = CategoryPage();
  final FavorisPage _favorisPage = FavorisPage();

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
  Widget pageChooser() {
    switch (this._currentIndex) {
      case 0:
        return _customHome;
        break;

      case 1:
        return _categoriesPage;
        break;

      case 2:
        return _favorisPage;
        break;
      case 3:
        return _profilePage;
        break;

      default:
        return new Container(
          child: new Center(
              child: new Text(
                  'No page found by page chooser.',
                  style: new TextStyle(fontSize: 30.0)
              )
          ),
        );
    }
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      body: pageChooser(),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset("images/all_icon.svg",
            color: _currentIndex == 0 ? Colors.red : Colors.black ),
            title: Text("All")
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset("images/category_icon.svg",
                color: _currentIndex == 1 ? Colors.red : Colors.black ),
              title: Text("Category")
          ),
          BottomNavigationBarItem(
              icon: SvgPicture.asset("images/favoris_icon.svg",
                  color: _currentIndex == 2 ? Colors.red : Colors.black ),
              title: Text("Favoris")
          ),
          BottomNavigationBarItem(
              icon: SvgPicture.asset("images/profile_icon.svg",
                  color: _currentIndex == 3 ? Colors.red : Colors.black ),
              title: Text("Profile")
          ),
        ],
      ),
    );
  }

}