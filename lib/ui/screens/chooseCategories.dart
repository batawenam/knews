import 'package:flutter/material.dart';
import 'package:kalinews/models/category.dart';
import 'package:kalinews/utils/categories.dart';
import 'package:kalinews/ui/widgets/categoryGridItem.dart';
import 'homeScreen.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';

class ChooseCategory extends StatefulWidget {
  ChooseCategoryState createState() => ChooseCategoryState();
}

class ChooseCategoryState extends State<ChooseCategory> {

  final databaseReference = FirebaseDatabase.instance.reference();
  final List<Category> _categories = getCategory();
  List<String> _selectedCategories = [];

  void pushSelectedCategory(Category category) {
    if(_selectedCategories.contains(category.categoryType)) {
      _selectedCategories.remove(category.categoryType);
      print(_selectedCategories.toString());
    } else {
      _selectedCategories.add(category.categoryType);
      print(_selectedCategories.toString());
    }
  }
  void goToDatabase () async{
    final user = await FirebaseAuth.instance.currentUser();
    String uid = user.uid.toString();
    databaseReference.child("/users/$uid/tags").set(_selectedCategories);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;
    Widget newBuild = StaggeredGridView.countBuilder(
      crossAxisCount: 4,
      itemCount: _categories.length,
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
      itemBuilder: (BuildContext context, int index) => new Container(
          child: GestureDetector(
            onTap: () {
              setState(() {
                _categories[index].categoryState =  !_categories[index].categoryState;
                pushSelectedCategory( _categories[index]);
              });
            },
            child: Container(
              child: CategoryItem( _categories[index]),
            ),
          ) ,
      ),
      staggeredTileBuilder: (int index) =>
      new StaggeredTile.count(2, index.isEven ? 2 : 3,),
      mainAxisSpacing: 6.0,
      crossAxisSpacing: 6.0,
    );
 /*   Widget buildSelectionGridView = GridView.count(
      childAspectRatio: (itemWidth / itemHeight),
      crossAxisCount: 2,
        controller: new ScrollController(keepScrollOffset: false),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
     padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
      children: List.generate(_categories.length, (index) {
        return index % 2 != 0 ? Container(
          margin: EdgeInsets.only(top: 20.0),
          child: GestureDetector(
            onTap: () {
              setState(() {
                _categories[index].categoryState =  !_categories[index].categoryState;
                pushSelectedCategory( _categories[index]);
              });
            },
            child: Container(
              child: CategoryItem( _categories[index]),
            ),
          ) ,
        )

        : Container(
          child:  GestureDetector(
            onTap: () {
              setState(() {
                _categories[index].categoryState =  !_categories[index].categoryState;
                pushSelectedCategory( _categories[index]);
              });
            },
            child: CategoryItem( _categories[index]),
          ),
        );
      })
      *//* _categories.map((category) {
        return GestureDetector(
          onTap: () {
            print(category);
            setState(() {
              category.categoryState = !category.categoryState;
              pushSelectedCategory(category);
            });
          },
          child: CategoryItem(category)
        );
      }).toList(),*//*
    );*/
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 50.0,),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Text(" Sélectionnez quelques catégories pour", style: TextStyle(fontFamily: "Roboto", fontSize: 18.0),),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.0),
            child: Text(" personnalier votre expérience", style: TextStyle(fontFamily: "Roboto", fontSize: 18.0),),
          ),
          Expanded(
              child: newBuild),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 22.0),
            child:MaterialButton(
                height: 60.0,
                elevation: 5.0,
                onPressed: () {
                      if(_selectedCategories.length < 3) {
                        print("ewo no bouging");
                      } else {
                        goToDatabase();
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage()));
                      }
                },
                color: _selectedCategories.length >= 3 ? Theme.of(context).primaryColor : Colors.white,
                shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                child: Center(
                  child: Text("COMMENCER", style: TextStyle(color: _selectedCategories.length >= 3 ? Colors.white :Theme.of(context).primaryColor ),),
                )
            ) ,
          ),
        ],
      )
    );
  }

}