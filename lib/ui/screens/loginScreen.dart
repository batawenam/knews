import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'chooseCategories.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kalinews/service/firebaseAuth.dart';
import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
class LoginScreen extends  StatefulWidget {

  LoginScreenState createState() =>  LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {

  //make google logo view function
  CircleAvatar _buildLogo () {
    return CircleAvatar(
      backgroundColor: Colors.transparent,
      backgroundImage: AssetImage("images/google.png"),
      radius: 10.0,
    );
  }
  Future<bool> _loginUser() async {
    final api = await FBApi.signInWithGoogle();
    if(api != null) {
      return true;
    } else {
      return false;
    }
  }
  Opacity _buildText() {
    return Opacity(
      opacity:1,
      child: Text(
        "Se connecter avec google",
        style: TextStyle(
            fontSize: 18.0,
            color: Colors.white
        ),
      ),

    );
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
  //  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset("images/login.png"),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 120.0, horizontal: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SvgPicture.asset("images/news.svg"),
                SizedBox(height: 20.0,),
                Text("L’essentiel de", style: TextStyle(fontSize: 40.0),),
                Text("l’actualité au",style: TextStyle(fontSize: 40.0)),
                Text("quotidien", style: TextStyle(fontSize: 40.0)),
                SizedBox(height: 50.0,),
                MaterialButton(
                    height: 60.0,
                    elevation: 5.0,
                    onPressed:  () async {
                      bool b = await _loginUser();
                      b
                      ? print(FirebaseAuth.instance.currentUser().toString())
                      : print("erreur c'est produites");
                     // Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChooseCategory()));
                    },
                    color: Theme.of(context).primaryColor,
                    shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                    child:SizedBox(
                      width: double.infinity,
                      child:  Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          _buildLogo(),
                          SizedBox(width: 14.0),
                          _buildText()
                        ],
                      ),
                    )
                ),
                SizedBox(height: 13.0),
                Opacity(
                  opacity: 0.6,
                  child: Text.rich(
                    TextSpan(
                      text: 'En continuant vous acceptez les ',
                      children: <TextSpan>[
                        TextSpan(
                          text: "conditions",
                          style: TextStyle(decoration: TextDecoration.underline)
                        )
                      ]
                    )
                  ),
                ),
                Opacity(
                    opacity: 0.6,
                    child: InkWell(
                      onTap: () async {
                        await launch("http://www.41devs.com");
                      },
                      child: Text("d’utilisation et de confidentialité",
                      style: TextStyle(decoration: TextDecoration.underline),),
                    ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}