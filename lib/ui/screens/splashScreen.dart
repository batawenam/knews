import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:kalinews/service/checklogged.dart';
import 'package:flutter/services.dart';

class MySplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MySplashScreenState();
  }
}

class MySplashScreenState extends State<MySplashScreen> {
  @override
  Widget build(BuildContext context) {
  //  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
    // TODO: implement build
    return
       SplashScreen(
        image: Image.asset("images/logo.png"),
        seconds: 5,
        photoSize: 100.0,
        backgroundColor: Theme.of(context).primaryColor,
        loaderColor: Colors.white,
         navigateAfterSeconds: CheckCurrentUser(),
    );
  }

}