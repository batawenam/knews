import 'package:flutter/material.dart';
import 'package:kalinews/service/firebaseAuth.dart';
import 'dart:async';

class Login extends StatelessWidget {
  Future<bool> _loginUser() async {
    final api = await FBApi.signInWithGoogle();
    if(api != null) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Sign In"),
        centerTitle: true,
      ),
      body:  Center(
        child:  Stack(
          children: <Widget>[
            RaisedButton(
              onPressed: () async {
                bool b = await _loginUser();

                b
                    ? print("Succes de la connection")
                    : print("erreur c'est produites");
              },
              child: Text("Log in"),
            )
          ],
        ),
      ),
    );
  }

}