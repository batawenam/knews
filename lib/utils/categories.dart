import 'package:kalinews/models/category.dart';

List<Category> getCategory() {
  return [
    Category(
      categoryImage: "images/default.png",
      categoryState: false,
      categoryType: "tech"
    ),
    Category(
        categoryImage: "images/default.png",
        categoryState: false,
        categoryType: "design"
    ),
    Category(
        categoryImage: "images/default.png",
        categoryState: false,
        categoryType: "finance"
    ),
    Category(
        categoryImage: "images/default.png",
        categoryState: false,
        categoryType: "divertissement"
    ),
    Category(
        categoryImage: "images/default.png",
        categoryState: false,
        categoryType: "marketing"
    ),
    Category(
        categoryImage: "images/default.png",
        categoryState: false,
        categoryType: "politique"
    ),
    Category(
        categoryImage: "images/default.png",
        categoryState: false,
        categoryType: "sante"
    ),
    Category(
        categoryImage: "images/default.png",
        categoryState: false,
        categoryType: "science"
    ),
    Category(
        categoryImage: "images/default.png",
        categoryState: false,
        categoryType: "sport"
    ),
  ];
}