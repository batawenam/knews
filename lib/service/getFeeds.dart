import 'package:firebase_database/firebase_database.dart';
import 'package:kalinews/models/post.dart';

final databaseReference = FirebaseDatabase.instance.reference();
Future<List<Post>> getData() {
  var list;
  databaseReference.child("/news").once().then((DataSnapshot dataSnapshot)
  {
      for (var value in dataSnapshot.value.values) {
        if(value != null) {
          list.add(Post.fromJson(value));
        }
      }
  });
  return list;
}