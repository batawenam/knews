import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kalinews/main.dart';
import 'package:kalinews/ui/screens/pages/homePage.dart';
import 'package:kalinews/ui/screens/homeScreen.dart';
import 'package:kalinews/ui/screens/loginScreen.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:kalinews/ui/screens/chooseCategories.dart';

final databaseReference = FirebaseDatabase.instance.reference();
class CheckCurrentUser extends StatefulWidget {
  CheckCurrentUserState createState() => CheckCurrentUserState();
}
class CheckCurrentUserState extends State<CheckCurrentUser> {
  Widget isOk;
  void checkData() async {
     if(FirebaseAuth.instance.currentUser()  != null ) {
      final FirebaseUser user = await FirebaseAuth.instance.currentUser();
      if(user != null) {
        final String uid = user.uid.toString();
        await databaseReference.child("/users/$uid").once().then((DataSnapshot data) {
          if(data.value != null) {
            setState(() {
              isOk = HomePage();
            });
          } else {
            setState(() {
              isOk = ChooseCategory();
            });
          }
        });
      } else {
        setState(() {
          this.isOk = LoginScreen();
        });
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    checkData();
    return isOk != null ? isOk : Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
        ),
      ),
    );
  }

}
