/*
import 'package:kalinews/utils/apiUrl.dart';
import 'package:kalinews/service/getFeeds.dart';
class MyModel {
  Duration _cacheValidDuration;
  DateTime _lastFetchTime;
  var _allRecords;
  var data;


  MyModel(){
    _cacheValidDuration = Duration(minutes: 30);
    _lastFetchTime = DateTime.fromMillisecondsSinceEpoch(0);
    _allRecords = null;
  }



  /// Refreshes all records from the API, replacing the ones that are in the cache.
  /// Notifies listeners if notifyListeners is true.
  Future<void> refreshAllRecords() async{
    _allRecords = await getFeedsJson(getDesignFeedsUrl); // This makes the actual HTTP request
    _lastFetchTime = DateTime.now();
    data = _allRecords["articles"];
   // if( notifyListeners ) this.notifyListeners();
  }


  /// If the cache time has expired, or forceRefresh is true, records are refreshed from the API before being returned.
  /// Otherwise the cached records are returned.
  Future<Map> getAllRecords({bool forceRefresh = false}) async{
    bool shouldRefreshFromApi = (null == _allRecords || _allRecords.isEmpty || null == _lastFetchTime || _lastFetchTime.isBefore(DateTime.now().subtract(_cacheValidDuration)) || forceRefresh);

    if( shouldRefreshFromApi )
      await refreshAllRecords();

    return data;
  }
}*/
