import 'package:flutter/material.dart';
import 'package:kalinews/ui/screens/splashScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:kalinews/ui/data/theme.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: buildTheme(),
      home:MySplashScreen(),
      routes: {

      },
    );
  }
}

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        children: <Widget>[
          Text("Sign out succes"),
          RaisedButton(
               onPressed: () {
                 FirebaseAuth.instance.signOut();
               },
             child: Text("Log Out"),
           )
        ],
      )
    );
  }

}