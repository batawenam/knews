import 'dart:core';

class Post {
  String id, title, url, imageUrl, content, time, source, category;

  Post({
    this.id,
    this.category,
    this.title,
    this.url,
    this.imageUrl,
    this.content,
    this.source,
    this.time
  });

  Post.fromJson(var databaseData) {
      this.id = databaseData['id'];
      this.title = databaseData['title'];
      this.source = databaseData['source'];
      this.content = databaseData['content'];
      this.category = databaseData['category'];
      this.url = databaseData['url'];
      this.imageUrl = databaseData['imageUrl'];
      this.time = databaseData['time'];
  }
}