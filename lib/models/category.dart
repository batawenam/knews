import 'dart:core';

enum CategoryType {
  TECH,
  SANTE,
  POLITIQUE,
  DESIGN,
  MARKETING,
  FINANCE,
  SCIENCE,
  SPORT,
  DIVERTISSEMENT
}
class Category {
   String categoryImage;
   String categoryType;
   bool  categoryState;

   Category({
    this.categoryImage,
    this.categoryType,
    this.categoryState
  });
}